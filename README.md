## Objective

Problem: Management is concerned about S3 buckets being publicly accessible to the internet. They want you to track situations like this and if you find any, they want to remediate them using Lambda. After the remediation is complete, they want you to send a notification to the security department about which buckets were remediated.

Solution: Configure AWS Config to track public buckets and create a Lambda function that will retrieve the list of non-compliant buckets from Config, make the non-compliant buckets private, and trigger and SNS notification to alert of remediation.

## **Architecture Diagram:**

![Project1](https://gitlab.com/b.owoyele/images/-/raw/main/Project1.png)

## **Solution:**

**Environment Setup**

1. Log into your AWS Console
2. Create public S3 buckets
   * In the S3 Dashboard select "Create Bucket"
   * Name the bucket & select a region
   * Remove the "Block all public access" configuration
   * Scroll to the bottom and select "create bucket"

**Remediation**

1. Go to the AWS Config dashboard and create a Config rule that identifies and tracks public S3 buckets (_Make you are in the same region as the buckets you created. Your config rule will only identify resources in the same region as the created rule)_
   * Select "Create rule"
   * Rule Type: AWS managed rule
   * Chosen Rule: s3-bucket-level-public-access-prohibited
   * Create AWS service-linked role
   * Delivery Method: Create a bucket
2. Go to the AWS SNS dashboard and create an SNS topic that will notify subscribers non-compliant public buckets have become private as a result of remediation by Lambda.
   * Select email as the endpoint.
   * Access Policy:
     * Publishers- account owner
     * Subscribers- everyone
   * Ensure that the email subscriber confirms subscription
3. Go to the Lambda dashboard and then create and code a lambda function that will perform the following tasks:
   * Fetch a list of non-compliant resources from Config
   * Change the access policies so that the non-compliant public S3 buckets will become private
   * Trigger an SNS notification that will alert of the completed remediation
        * See example code in the repository
4. Go to the IAM dashboard and create an appropriate IAM policy permitting your Lambda function to access to Config, S3, and SNS resources (apply principle of LEAST privilege when creating your policy).
5. Attach the policy to an IAM role, and attached the IAM role to Lambda.
6. Test your Lambda function to ensure that the desired remediations are being performed by either creating a public S3 bucket or by changing an existing S3 bucket access to public.
