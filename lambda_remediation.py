import json
import boto3

s3_client = boto3.client('s3')
sns_client = boto3.client('sns')
Config_client = boto3.client('config')
def lambda_handler(event, context):
    #get all non compliant resources from config
    response = Config_client.get_compliance_details_by_config_rule(
        ConfigRuleName='s3-bucket-level-public-access-prohibited',
        ComplianceTypes=['NON_COMPLIANT'],
        )
    non_compliant= json.loads(json.dumps(response, default=str))
    eval_result=non_compliant.get('EvaluationResults')

    for item in eval_result:
        non_comp_bucket = item.get('EvaluationResultIdentifier').get('EvaluationResultQualifier').get('ResourceId')
        print(non_comp_bucket)
        #block the bucket
        response = s3_client.put_public_access_block(
        Bucket= non_comp_bucket,
        PublicAccessBlockConfiguration={
            "BlockPublicAcls": True,
            "IgnorePublicAcls": True,
            "BlockPublicPolicy": True,
            "RestrictPublicBuckets": True
        })
          
        #send SNS notification
        response = sns_client.publish (
              TopicArn = "arn:aws:sns:us-east-1:292460448336:S3_bucket_remediation",
              Message = "Public buckets have been remediated",
              Subject = 'S3 bucket Remediation'
              )
     
    return " I have found " + str(len(eval_result)) + 'resources that are noncompliant and have been remediated. An SNS notification has been sent notifying of completed remediation'
